<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Richard Frank's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <header>
        <?php include ('template/header.php'); ?>
    </header>
    <nav>
        <?php include ('template/nav.php'); ?>
    </nav>
    <main>
        <img src="img/portrait.jpg" alt="Portrait" width="150" height="150"/>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas viverra hendrerit erat, et viverra lacus auctor eu. Mauris urna lectus, faucibus id malesuada pharetra, mattis eget lectus. Sed a lorem faucibus, consequat dolor sed, efficitur risus. Quisque molestie porta suscipit. In elementum enim eget nibh rhoncus mollis. Aenean lobortis aliquet velit at accumsan. Vestibulum auctor euismod risus ac dictum. In vitae augue sodales, tristique lorem at, malesuada justo. Praesent maximus nulla vitae purus molestie, ut ultricies nunc semper. Pellentesque tincidunt sem metus, non luctus ligula consequat mollis. Aenean sed cursus libero. Duis eu tortor id libero mollis commodo id id nunc. Donec malesuada finibus velit, vel tempus arcu ultricies facilisis. Duis sed nisl a enim viverra tincidunt in pretium ex</p>
    </main>
    <footer>
        <?php include ('template/footer.php'); ?>
    </footer>
</body>
</html>

